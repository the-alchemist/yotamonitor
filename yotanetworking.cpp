#include "yotanetworking.h"

YotaWorker::YotaWorker(QObject *parent) :
    QObject(parent)
{

}

/// SLOT
void YotaWorker::status()
{
    manager.get(QNetworkRequest(yotaStatusUrl));
}

/// SLOT
void YotaWorker::info()
{
    manager.get(QNetworkRequest(yotaInfoUrl));
}

void YotaWorker::toggleLed()
{
    QNetworkRequest req;
    QNetworkReply *reply;

    QByteArray postData;

    statusLed = getStatusLed(); //get from json
    //http://10.0.0.1/devcontrol?callback=abe&command=getMobileSettings

    postData.append("callback=dddd");
    postData.append("&command=");
    postData.append(command.SET_MOBILE_SETTINGS);
    postData.append("&logoLedControl=");
    postData.append(QString::number(getStatusLed()));

    req.setUrl(QUrl("http://10.0.0.1:80/devcontrol"));
    req.setRawHeader("Content-Type", "application/x-www-form-urlencoded");

    qDebug() << "post query: " << req.url().toString() << QString::fromStdString(postData.toStdString());

    reply = manager.post(req, postData);

    if(reply->error() == QNetworkReply::NoError)
    {
        qDebug() << "enable led ok";
        qDebug() << "reply: " << reply->readAll();
    }
}

///FIXME
void YotaWorker::networkUsersCount()
{
    QNetworkRequest req;
    QNetworkReply *reply;

    QByteArray postData, response;
    postData.append("callback=NetworkUsersCount");
    postData.append("&command=");
    postData.append(command.GET_MOBILE_SETTINGS);

    req.setUrl(QUrl("http://10.0.0.1:80/devcontrol?callback=abc"));
    req.setRawHeader("Content-Type", "application/x-www-form-urlencoded");

    reply = manager.post(req, postData);

    if(reply->error() == QNetworkReply::NoError)
    {
        response = reply->readAll();
    }

    emit listenUsersConnected(response);
}

void YotaWorker::changeNetworkUsersCount()
{
    QNetworkRequest req;
    QNetworkReply *reply;

    //protectedUsers = getProtectedUsers();
    //openUsers = getOpenUsers();
    //from json


    QByteArray postData;
    postData.append("cfgwifi_maxSta1=");
    postData.append(QString::number(protectedUsers).toUtf8());
    postData.append("&cfgwifi_maxSta2=");
    postData.append(QString::number(openUsers).toUtf8());
    postData.append("&command=");
    postData.append(command.SET_WEB_UI_CONFIG);


    req.setUrl(QUrl("http://10.0.0.1:80/devcontrol?callback=jQuery" + QString::number(QDateTime::currentMSecsSinceEpoch())).toString());

    qDebug() << "post query: "
             << req.url().toString()
             << QString::fromStdString(postData.toStdString());

    req.setRawHeader("Content-Type", "application/x-www-form-urlencoded");

    reply = manager.post(req, postData);

    if(reply->error() == QNetworkReply::NoError)
    {
        qDebug() << "changing users ok";
        qDebug() << "reply: " << reply->readAll();
    }

}

void YotaWorker::process()
{
    QNetworkReply *statusReply;

    statusReply = manager.get(QNetworkRequest(yotaStatusUrl));

    QByteArray data;
    data = statusReply->readAll();

    emit dataFetched(data);
    qApp->processEvents();
}

int YotaWorker::getStatusLed() const
{
    return statusLed;
}

void YotaWorker::setStatusLed(int value)
{
    statusLed = value;
}

void YotaWorker::setOpenUsers(int value)
{
    openUsers = value;
}

void YotaWorker::setProtectedUsers(int value)
{
    protectedUsers = value;
}

