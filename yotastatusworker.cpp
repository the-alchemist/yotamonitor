#include "yotastatusworker.h"

YotaStatusWorker::YotaStatusWorker()
{

}

void YotaStatusWorker::process()
{
    while(running)
    {
        //update status
        loadStatus();
        qApp->processEvents();
    }
    emit finished();
}

void YotaStatusWorker::stop()
{
    running = false;
}

void YotaStatusWorker::loadStatus()
{
    QByteArray data;
    QNetworkReply *reply;

    reply = manager.get(QNetworkRequest(yotaStatusUrl));
    data = reply->readAll();

    emit loadedStatus(data);
}
