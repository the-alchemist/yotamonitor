#ifndef YOTASTATUSWORKER_H
#define YOTASTATUSWORKER_H

#include <QObject>
#include <QApplication>
#include <QNetworkAccessManager>
#include <QNetworkReply>

#include <QUrl>

#include "yotacommand.h"
class YotaStatusWorker : public QObject
{
    Q_OBJECT
public:
    YotaStatusWorker();

public slots:
    void process();
    void stop();

signals:
    void loadedStatus(QByteArray);
    void post();
    void finished();

private:
    void loadStatus();

private:
    bool running;
    QNetworkAccessManager manager;
    YotaCommand command;
    QUrl yotaStatusUrl = QUrl("http://10.0.0.1:80/devcontrol?callback=Status&command=" + command.GET_STATUS);

};

#endif // YOTASTATUSWORKER_H
