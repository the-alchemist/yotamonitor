#include "yotasinrdiagramwidget.h"
#include "ui_yotasinrdiagramwidget.h"

YotaSinrDiagramWidget::YotaSinrDiagramWidget(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::YotaSinrDiagramWidget)
{
    ui->setupUi(this);

    QGraphicsScene *scene = new QGraphicsScene(ui->graphicsViewDiagram);

    QPen pen(Qt::gray);

    int plotWidth = ui->graphicsViewDiagram->width();
    int plotHeight = ui->graphicsViewDiagram->height();

    scene->addLine(1, 1,
                   plotWidth,  1,
                   pen);
    for(int x = 0; x < plotWidth; x += 10)
    {
        scene->addLine(x, x+10,
                       plotWidth, x+10,
                       pen);
    }



    ui->graphicsViewDiagram->setScene(scene);


}

YotaSinrDiagramWidget::~YotaSinrDiagramWidget()
{
    delete ui;
}

void YotaSinrDiagramWidget::on_pushButtonCloseWindow_clicked()
{
    this->close();
}
