#ifndef YOTAPREFERENCES_H
#define YOTAPREFERENCES_H

#include <QString>

class YotaPreferences
{
private:
    bool connected;
    int maxReceiveSpeed,
    maxTransmitSpeed,
    currentReceiveSpeed,
    currentTransmitSpeed;

    int sinr, rsrp;

    QString batteryStatus;
    int batteryLevel;

public:
    YotaPreferences();
    YotaPreferences(const YotaPreferences& obj);
    YotaPreferences(bool isConnected, int maxRx, int maxTx,
                    int currentRx, int currentTx, int sinr,
                    int rsrp);

    //isConnected getters/setters
    bool isConnected() const;
    void setIsConnected(const bool &value);

    //max and current speed setters
    void setMaxSpeed(int receive, int transmit);

    void setCurrentSpeed(int receive, int transmit);

    //maxReceiveSpeed getters/setters
    int getMaxReceiveSpeed() const;
    void setMaxReceiveSpeed(int value);

    //maxTransmitSpeed getters/setters
    int getMaxTransmitSpeed() const;
    void setMaxTransmitSpeed(int value);

    //currentReceiveSpeed getters/setters
    int getCurrentReceiveSpeed() const;
    void setCurrentReceiveSpeed(int value);

    //currentTransmitSpeed getters/setters
    int getCurrentTransmitSpeed() const;
    void setCurrentTransmitSpeed(int value);

    //sinr getters/setters
    int getSinr() const;
    void setSinr(int value);

    //rsrp getters/setters
    int getRsrp() const;
    void setRsrp(int value);

    QString getBatteryStatus() const;
    void setBatteryStatus(const QString &value);

    int getBatteryLevel() const;
    void setBatteryLevel(int value);
};

#endif // YOTAPREFERENCES_H
