#include "yotapreferences.h"

bool YotaPreferences::isConnected() const
{
    return connected;
}

void YotaPreferences::setIsConnected(const bool &value)
{
    connected = value;
}


void YotaPreferences::setMaxSpeed(int receive, int transmit)
{
    setMaxReceiveSpeed(receive);
    setMaxTransmitSpeed(transmit);
}


void YotaPreferences::setCurrentSpeed(int receive, int transmit)
{
    setCurrentReceiveSpeed(receive);
    setCurrentTransmitSpeed(transmit);
}


int YotaPreferences::getMaxTransmitSpeed() const
{
    return maxTransmitSpeed;
}

void YotaPreferences::setMaxTransmitSpeed(int value)
{
    maxTransmitSpeed = value;
}

int YotaPreferences::getMaxReceiveSpeed() const
{
    return maxReceiveSpeed;
}

void YotaPreferences::setMaxReceiveSpeed(int value)
{
    maxReceiveSpeed = value;
}

int YotaPreferences::getCurrentReceiveSpeed() const
{
    return currentReceiveSpeed;
}

void YotaPreferences::setCurrentReceiveSpeed(int value)
{
    currentReceiveSpeed = value;
}

int YotaPreferences::getCurrentTransmitSpeed() const
{
    return currentTransmitSpeed;
}

void YotaPreferences::setCurrentTransmitSpeed(int value)
{
    currentTransmitSpeed = value;
}

int YotaPreferences::getSinr() const
{
    return sinr;
}

void YotaPreferences::setSinr(int value)
{
    sinr = value;
}


int YotaPreferences::getRsrp() const
{
    return rsrp;
}

void YotaPreferences::setRsrp(int value)
{
    rsrp = value;
}


QString YotaPreferences::getBatteryStatus() const
{
    return batteryStatus;
}

void YotaPreferences::setBatteryStatus(const QString &value)
{
    batteryStatus = value;
}

int YotaPreferences::getBatteryLevel() const
{
    return batteryLevel;
}

void YotaPreferences::setBatteryLevel(int value)
{
    batteryLevel = value;
}

YotaPreferences::YotaPreferences()
{

}
