#include "yotamonitorwidget.h"
#include "ui_yotamonitor.h"

YotaMonitorWidget::YotaMonitorWidget(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::YotaMonitor)
{
    ui->setupUi(this);

    /*** MainWindow ***/

    //  this->setWindowFlags(Qt::FramelessWindowHint); //hide form controls
    //  this->setAttribute(Qt::WA_TranslucentBackground); //translucent background

    /**************/


    /**** TRAY ****/

    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setIcon(QIcon(":/yota/assets/logo.png"));
    trayIcon->setToolTip("Yota Monitor");
    trayIcon->show();

    /**************/

    ui->comboBoxRefreshType->addItem("secs");
    ui->comboBoxRefreshType->addItem("msecs");

    /**** NETWORK THREAD INIT ****/

    statusThread = new QThread;
    statusWorker = new YotaStatusWorker;
    statusWorker->moveToThread(statusThread);

    connect(statusThread, &QThread::started, statusWorker, &YotaStatusWorker::process);
    connect(statusWorker, &YotaStatusWorker::finished, statusThread, &QThread::quit);
    connect(statusWorker, &YotaStatusWorker::finished, statusWorker, &YotaStatusWorker::deleteLater);
    connect(statusWorker, &YotaStatusWorker::finished, statusWorker, &YotaStatusWorker::stop);
    connect(statusThread, &QThread::finished, statusThread, &QThread::deleteLater);
    connect(statusWorker, &YotaStatusWorker::loadedStatus, this, &YotaMonitorWidget::parseData);

    statusThread->start();
    /*onnect(workerThread, SIGNAL(started()), worker, SLOT(doWork()));
    connect(worker, SIGNAL(finished()), workerThread, SLOT(quit()));
    connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
    connect(worker, SIGNAL(finished()), this, SLOT(countFinished()));
    connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));
    connect(worker, SIGNAL(updateCount(int)), this, SLOT(updateCount(int)));*/

    /**************/


    //connect(yota, SIGNAL(finished()), this, SLOT(parseData(QByteArray)));
   // connect(yota, &YotaWorker::listenUsersConnected, this, &YotaMonitorWidget::getUsersConnected);

    //connect(&timer, &QTimer::timeout, yota, &YotaWorker::load);

    //connect(&timer, &QTimer::timeout, statusThread, &QThread::start);
    //connect(&timer, &QTimer::timeout, yota, &YotaWorker::info);

    connect(&timer, &QTimer::timeout, this, &YotaMonitorWidget::updateTime);
    connect(&timer, &QTimer::timeout, this, &YotaMonitorWidget::checkGoodSinr);
    connect(&timer, &QTimer::timeout, this, &YotaMonitorWidget::checkGoodRsrp);
    connect(&timer, &QTimer::timeout, this, &YotaMonitorWidget::updateTrayInfo);

    timer.start();

    connect(trayIcon, &QSystemTrayIcon::activated, this, &YotaMonitorWidget::toTray);

    //connect(ui->radioButtonEnableLED, &QAbstractButton::clicked, yota, &YotaWorker::toggleLed);
    //connect(ui->pushButtonApplyNumUsers, &QAbstractButton::clicked, yota, &YotaWorker::changeNetworkUsersCount);
}

YotaMonitorWidget::~YotaMonitorWidget()
{
    delete ui;
}

void YotaMonitorWidget::setPreferences(QJsonObject json)
{
    if(type.contains("Status("))
    {
        prefs.setIsConnected(json.value("connected").toBool());
        prefs.setCurrentSpeed(json.value("currRxSpeed").toInt(), json.value("currTxSpeed").toInt());
        prefs.setMaxSpeed(json.value("maxSessRxSpeed").toInt(), json.value("maxSessTxSpeed").toInt());
        prefs.setSinr(json.value("sinr").toInt());
        prefs.setRsrp(json.value("rsrp").toInt());
    }

    if(type.contains("Info("))
    {
        prefs.setBatteryLevel(json.value("battLevel").toInt());
        prefs.setBatteryStatus(json.value("battCharging").toString());
    }

}

YotaMonitorWidget::SignalStates YotaMonitorWidget::isGoodRsrp(int rsrp)
{
    if(rsrp >= -80)
    {
        return EXCELLENT;
    }
    else if(rsrp < -80 && rsrp >= -90)
    {
        return GOOD;
    }
    else if(rsrp < -90 && rsrp >= -100)
    {
        return NORMAL;
    }
    else
    {
        return BAD;
    }
}

YotaMonitorWidget::SignalStates YotaMonitorWidget::isGoodSinr(int sinr)
{
    if(sinr >= 20)
    {
        return EXCELLENT;
    }
    else if(sinr < 20 && sinr >= 13)
    {
        return GOOD;
    }
    else if(sinr < 13 && sinr >= 0)
    {
        return NORMAL;
    }
    else
    {
        return BAD;
    }
}


void YotaMonitorWidget::showPreferences(QJsonObject json)
{
    if(type.contains("Status("))
    {
        ui->labelIPAddressVal->setText(json.value("ip").toString());
        ui->labelSessionDurationVal->setText(QDateTime::fromTime_t(json.value("sessionDuration").toInt()).toUTC().toString("hh:mm:ss"));
        ui->labelStatusVal->setText(prefs.isConnected() ? "connected" : "not connected");

        double receive = formatter.autoFormat(prefs.getCurrentReceiveSpeed());
        double transmit = formatter.autoFormat(prefs.getCurrentTransmitSpeed());
        double maxReceive = formatter.autoFormat(prefs.getMaxReceiveSpeed());
        double maxTransmit = formatter.autoFormat(prefs.getMaxTransmitSpeed());

        ui->labelCurrentSpeedReceiveVal->setText("↓ " + QString::number(receive) + " " + formatter.getCurrentFormat());
        ui->labelCurrentSpeedTransmitVal->setText("↑ " + QString::number(transmit) + " " + formatter.getCurrentFormat());

        ui->labelMaxSpeedReceiveVal->setText("↓ " + QString::number(maxReceive) + " " + formatter.getCurrentFormat());
        ui->labelMaxSpeedTransmitVal->setText("↑ " + QString::number(maxTransmit) + " " + formatter.getCurrentFormat());

        ui->labelSINRVal->setText(QString::number(prefs.getSinr()) + " dB");
        ui->labelRSRPVal->setText(QString::number(prefs.getRsrp()) + " dBm");
    }
    if(type.contains("Info("))
    {
        ui->labelBatteryStatus->setText(prefs.getBatteryStatus());
        ui->progressBarBatteryLevel->setValue(prefs.getBatteryLevel());
    }

}


/// SLOT ///
void YotaMonitorWidget::parseData(QByteArray data)
{
    type = QString::fromStdString(data.mid(0, 7).toStdString());
    if(type.contains("Status("))
    {
        data.replace("Status(", "");

    }
    if(type.contains("Info("))
    {
        data.replace("Info(", "");
    }
    data.replace(")", "");


    QJsonDocument doc = QJsonDocument::fromJson(data);
    QJsonObject json = doc.object();

    //qDebug() << "rsrp: " << jsonObject.value("rsrp").toInt();
    //qDebug () << "[parseData - type]:" << type;
    setPreferences(json);
    showPreferences(json);

}

void YotaMonitorWidget::getUsersConnected(QByteArray data)
{
    type = QString::fromStdString(data.mid(0,4).toStdString());

    if(type.contains("abc("))
    {
        data.replace("abc(", "");
    }
    data.replace(")", "");

    QJsonDocument doc = QJsonDocument::fromJson(data);
    QJsonObject json = doc.object();

    ui->labelUsersProtectedNetwork->setText(json.value("cfgwifi_maxSta1").toString());
    ui->labelUsersFreeNetwork->setText(json.value("cfgwifi_maxSta2").toString());

}

/// SLOT ///
void YotaMonitorWidget::updateTrayInfo()
{
    double receive = formatter.autoFormat(prefs.getCurrentReceiveSpeed());
    double transmit = formatter.autoFormat(prefs.getCurrentTransmitSpeed());

    QString trayToolTip;
    trayToolTip.append("Yota Monitor v.0.0.1 preview");

    trayToolTip.append("\nReceive speed: ");
    trayToolTip.append(QString::number(receive));
    trayToolTip.append(" ");
    trayToolTip.append(formatter.getCurrentFormat());

    trayToolTip.append("\nTransmit speed: ");
    trayToolTip.append(QString::number(transmit));
    trayToolTip.append(" ");
    trayToolTip.append(formatter.getCurrentFormat());


    trayToolTip.append("\nSINR: ");
    trayToolTip.append(QString::number(prefs.getSinr()));
    trayToolTip.append(" dB");

    trayToolTip.append("\nRSRP: ");
    trayToolTip.append(QString::number(prefs.getRsrp()));
    trayToolTip.append(" dBm");

    trayIcon->setToolTip(trayToolTip);
}

/*void YotaMonitorWidget::updateInfo(QNetworkReply *reply)
{
    if(!reply->error())
    {
        QByteArray data = reply->readAll();
        data.replace("lorem(", "");
        data.replace(")", "");

        qDebug() << "\t\tFormatted JSON:\n" << data << "\n";
        QJsonDocument doc = QJsonDocument::fromJson(data);
        QJsonObject json = doc.object();

        qDebug() << "rsrp: " << json.value("rsrp").toInt();

        showPreferences(json);

    } else {
        QMessageBox::warning(this, "Ошибка!", "Не удалось установить подключение к сети");
        return;
    }

    delete reply;
}*/

/// SLOT ///
void YotaMonitorWidget::refreshValueChanged(int value)
{
    timer.stop();
    switch(ui->comboBoxRefreshType->currentIndex())
    {
    case 0:
        timer.setInterval(value *= 1000);
        break;
    case 1:
        timer.setInterval(value);
    default:
        break;
    }
    timer.start();
}

/// SLOT ///
void YotaMonitorWidget::updateTime()
{
    QDateTime dateTime = QDateTime::currentDateTime();
    currentTimeMsecs = dateTime.toTime_t();
    ui->labelCurrentTimeSecs->setText("Сегодня" + QDateTime::fromTime_t(currentTimeMsecs).toString(" dddd, dd.MM.yyyy hh:mm:ss"));
}

/// SLOT ///
void YotaMonitorWidget::checkGoodSinr()
{
    sinrStates = isGoodSinr(prefs.getSinr());

    switch(sinrStates)
    {

    case EXCELLENT:
        ui->labelSINRVal->setStyleSheet("color: green;");
        break;

    case GOOD:
        ui->labelSINRVal->setStyleSheet("color: teal;");
        break;

    case NORMAL:
        ui->labelSINRVal->setStyleSheet("color: orange;");
        break;

    case BAD:
        ui->labelSINRVal->setStyleSheet("color: red;");
        break;

    default:
        ui->labelSINRVal->setStyleSheet("color: #444;");
        break;

    }
}

/// SLOT ///
void YotaMonitorWidget::checkGoodRsrp()
{
    rsrpStates = isGoodRsrp(prefs.getRsrp());

    switch(rsrpStates)
    {

    case EXCELLENT:
        ui->labelRSRPVal->setStyleSheet("color: green;");
        break;

    case GOOD:
        ui->labelRSRPVal->setStyleSheet("color: teal;");
        break;

    case NORMAL:
        ui->labelRSRPVal->setStyleSheet("color: orange;");
        break;

    case BAD:
        ui->labelRSRPVal->setStyleSheet("color: red;");
        break;

    default:
        ui->labelRSRPVal->setStyleSheet("color: #444;");
        break;

    }
}

void YotaMonitorWidget::toTray(QSystemTrayIcon::ActivationReason reason)
{
    if(reason == QSystemTrayIcon::Trigger)
    {
        if(this->isVisible())
            this->show();
        else
            this->hide();
    }
}

void YotaMonitorWidget::on_sliderUsersNetwork_valueChanged(int value)
{
    ui->pushButtonApplyNumUsers->setVisible(true);
    ui->labelUsersProtectedNetwork->setText(QString::number(value));
    ui->labelUsersFreeNetwork->setText(QString::number(ui->sliderUsersNetwork->maximum() - value));
}

void YotaMonitorWidget::on_pushButtonApplyNumUsers_clicked()
{
    //yota->setOpenUsers(ui->labelUsersFreeNetwork->text().toInt());
    //yota->setProtectedUsers(ui->labelUsersProtectedNetwork->text().toInt());

    ui->pushButtonApplyNumUsers->setVisible(false);
}

void YotaMonitorWidget::on_pushButtonShowSinrDiagram_clicked()
{
    diagramWidget->show();
}

void YotaMonitorWidget::on_pushButtonCloseWidget_clicked()
{
    this->close();
}

void YotaMonitorWidget::mouseMoveEvent(QMouseEvent *moveEvent)
{
    if(currentPosition.x() >= 0 && moveEvent->buttons() && Qt::LeftButton)
    {
        QPoint difference = moveEvent->pos() - currentPosition;
        QPoint newPosition = this->pos() + difference;
        this->move(newPosition);
    }
}

void YotaMonitorWidget::mousePressEvent(QMouseEvent *pressEvent)
{
    currentPosition = pressEvent->pos();
}

void YotaMonitorWidget::mouseReleaseEvent(QMouseEvent *releaseEvent)
{
    currentPosition = QPoint(-1, 1);
}
