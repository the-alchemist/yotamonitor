#include "bytesformatter.h"

void BytesFormatter::setCurrentFormat(QString fmt)
{
    currentFormat = fmt;
}

double BytesFormatter::toKilobitsPerSecond(unsigned int bitsPerSecond)
{
    return bitsPerSecond * 0.001;
}

double BytesFormatter::toMegabitsPerSecond(unsigned int bitsPerSecond)
{
    return toKilobitsPerSecond(bitsPerSecond) * 0.001;
}

double BytesFormatter::toGigabitsPerSecond(unsigned int bitsPerSecond)
{
    return toMegabitsPerSecond(bitsPerSecond) * 0.001;
}

double BytesFormatter::autoFormat(unsigned int bps)
{
    if(bps < 1000)
    {
        setCurrentFormat("bps");
        return bps;
    }
    else if(bps >= 1000)
    {
        setCurrentFormat("Kbps");
        return toKilobitsPerSecond(bps);
    }
    else if(toKilobitsPerSecond(bps) > 1000)
    {
        setCurrentFormat("Mbps");
        return toMegabitsPerSecond(bps);
    }
    else if(toMegabitsPerSecond(bps) > 1000)
    {
        setCurrentFormat("Gbps");
        return toGigabitsPerSecond(bps);
    }
    else
    {
      setCurrentFormat("bps");
    }
    return bps;
}

QString BytesFormatter::getCurrentFormat()
{
    return currentFormat;
}
