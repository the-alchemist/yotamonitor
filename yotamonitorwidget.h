#ifndef YOTAMONITOR_H
#define YOTAMONITOR_H

#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QUrlQuery>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMessageBox>
#include <QTimer>
#include <QDateTime>
#include <QTime>
#include <QSystemTrayIcon>
#include <QtGui>
#include <QThread>

#include "yotastatusworker.h"
#include "yotapreferences.h"
#include "bytesformatter.h"
#include "yotasinrdiagramwidget.h"

namespace Ui {
class YotaMonitor;
}

class YotaMonitorWidget : public QMainWindow
{
    Q_OBJECT

/// QT SYSTEM UI ///
private:
    Ui::YotaMonitor *ui;

/// QT OBJECTS ///
private:
    QString type;
    QSystemTrayIcon *trayIcon;
    QThread *statusThread, *infoThread;
    QTimer timer;

/// USER VARIABLES ///
private:
    enum SignalStates { EXCELLENT, GOOD, NORMAL, BAD } States;
    long int currentTimeMsecs = 0;

/// USER OBJECTS ///
private:
    BytesFormatter formatter;
    YotaPreferences prefs;
    YotaSinrDiagramWidget *diagramWidget;
    YotaStatusWorker *statusWorker;

    SignalStates sinrStates;
    SignalStates rsrpStates;

    QPoint currentPosition;

public:
    explicit YotaMonitorWidget(QWidget *parent = 0);
    ~YotaMonitorWidget();

/// USER FUNCTIONS ///
private:
    void showPreferences(QJsonObject jsonObject);
    void setPreferences(QJsonObject jsonObject);

    SignalStates isGoodRsrp(int rsrp);
    SignalStates isGoodSinr(int sinr);

/// USER SLOTS ///
private slots:
    void checkGoodSinr();
    void checkGoodRsrp();
    void getUsersConnected(QByteArray data);
    void parseData(QByteArray data);
    void refreshValueChanged(int value);
    void toTray(QSystemTrayIcon::ActivationReason reason);
    void updateTime();
    void updateTrayInfo();

/// SYSTEM GENERATED SLOTS ///
private slots:
    void on_sliderUsersNetwork_valueChanged(int value);
    void on_pushButtonApplyNumUsers_clicked();
    void on_pushButtonShowSinrDiagram_clicked();
    void on_pushButtonCloseWidget_clicked();

/// EVENTS ///
private slots:
    void mouseMoveEvent(QMouseEvent *moveEvent);
    void mousePressEvent(QMouseEvent *pressEvent);
    void mouseReleaseEvent(QMouseEvent *releaseEvent);

};

#endif // YOTAMONITOR_H
