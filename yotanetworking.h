#ifndef YOTANETWORKING_H
#define YOTANETWORKING_H

#include <QApplication>
#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QUrl>
#include <QUrlQuery>
#include <QString>
#include "yotacommand.h"

class YotaWorker : public QObject
{
    Q_OBJECT
public:

private:

    YotaCommand command;
    QNetworkAccessManager manager;

    bool statusLed;

    int openUsers = 0;
    int protectedUsers = 1;

    QUrl yotaStatusUrl = QUrl("http://10.0.0.1:80/devcontrol?callback=Status&command=" + command.GET_STATUS);
    QUrl yotaInfoUrl = QUrl("http://10.0.0.1:80/devcontrol?callback=Info&command=" + command.GET_GENERAL_INFO);
    QUrl yotaDevControlUrl = QUrl("http://10.0.0.1:80/devcontrol?callback=abc");

public:
    explicit YotaWorker(QObject *parent = 0);

    int getStatusLed() const;
    void setStatusLed(int value);

    void setOpenUsers(int value);
    void setProtectedUsers(int value);

signals:
    void dataFetched(QByteArray);
    void listenUsersConnected(QByteArray);

public slots:
    void process();

private slots:
    void status();
    void info();
    void toggleLed();
    void networkUsersCount();
    void changeNetworkUsersCount();
};

#endif // YOTANETWORKING_H
