#ifndef YOTACOMMAND_H
#define YOTACOMMAND_H

#include <QObject>

class YotaCommand
{
public:
    const QString GET_STATUS = "getStatus";
    const QString GET_GENERAL_INFO = "getGeneralInfo";
    const QString GET_MOBILE_SETTINGS = "getMobileSettings";
    const QString SET_MOBILE_SETTINGS = "setMobileSettings";
    const QString GET_WEB_UI_CONFIG = "getWebUiConfig";
    const QString SET_WEB_UI_CONFIG = "setWebUiConfig";
public:
    YotaCommand();
};

#endif // YOTACOMMAND_H
