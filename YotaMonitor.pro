#-------------------------------------------------
#
# Project created by QtCreator 2016-09-12T15:44:08
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = YotaMonitor
TEMPLATE = app


SOURCES += main.cpp\
    yotapreferences.cpp \
    yotanetworking.cpp \
    yotamonitorwidget.cpp \
    bytesformatter.cpp \
    yotasinrdiagramwidget.cpp \
    yotacommand.cpp \
    yotastatusworker.cpp

HEADERS  += \
    yotapreferences.h \
    yotanetworking.h \
    yotamonitorwidget.h \
    bytesformatter.h \
    yotasinrdiagramwidget.h \
    yotacommand.h \
    yotastatusworker.h

FORMS    += yotamonitor.ui yotasinrdiagramwidget.ui

RESOURCES += yotamonitor.qrc
