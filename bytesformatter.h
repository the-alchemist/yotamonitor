#ifndef BYTESFORMATTER_H
#define BYTESFORMATTER_H

#include <QString>
#include "math.h"

class BytesFormatter
{
private:
    QString currentFormat;
    void setCurrentFormat(QString fmt);
public:
    double toKilobitsPerSecond(unsigned int bytes);
    double toMegabitsPerSecond(unsigned int bytes);
    double toGigabitsPerSecond(unsigned int bytes);
    double autoFormat(unsigned int bytes);

    QString getCurrentFormat();
};

#endif // BYTESFORMATTER_H
