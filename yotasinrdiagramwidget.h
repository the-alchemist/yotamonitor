#ifndef YOTASINRDIAGRAMWIDGET_H
#define YOTASINRDIAGRAMWIDGET_H

#include <QMainWindow>

namespace Ui {
class YotaSinrDiagramWidget;
}

class YotaSinrDiagramWidget : public QMainWindow
{
    Q_OBJECT

public:
    explicit YotaSinrDiagramWidget(QWidget *parent = 0);
    ~YotaSinrDiagramWidget();

private slots:
    void on_pushButtonCloseWindow_clicked();

private:
    Ui::YotaSinrDiagramWidget *ui;
};

#endif // YOTASINRDIAGRAMWIDGET_H
