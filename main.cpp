#include "yotamonitorwidget.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    YotaMonitorWidget w;
    w.show();

    return a.exec();
}
